<?php
require "vendor/autoload.php";

function IsIPInDatabase($ip, $date)
{
    $total = Model::factory('Sondage')->where(array(
        'date' => $date,
        'ip' => $ip
    ))->count();
    if($total > 0)
    {
        return True;
    }
    return False;
    
}