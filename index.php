<?php 
require "vendor/autoload.php";

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
    $twigConfig = array(
        // 'cache' => './cache/twig/',
        // 'cache' => false,
        'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
});

Flight::before('start', function(&$params, &$output){
    ORM::configure('sqlite:RemyHour.sqlite3');
});

Flight::route('/RemyHour', function(){
    /*var_dump(Flight::request());*/ /*pour affichage de test*/
    Flight::view()->display('remyhour.twig');
});

Flight::route('/RemyHour/Contact', function(){
    Flight::view()->display('contact.twig');
});


Flight::route('/RemyHour/Resultats', function(){
    $data = [
        'sondages' => Sondage::find_many(),
    ];
    Flight::view()->display('resultats.twig', $data);
});

Flight::route('/RemyHour/Resultats/add', function(){
    
    if (Flight::request()->method == 'POST'){
        $ip = Flight::request()->ip;
        $date = Flight::request()->data->date;

        if(!IsIPInDatabase($ip, $date)){
            $sondage = Model::factory('Sondage')->create();
            $sondage->pseudo = Flight::request()->data->pseudo;
            $sondage->date = Flight::request()->data->date;
            $sondage->heure = Flight::request()->data->heure;
            $sondage->ip = $ip;
            $sondage->date = $date;
    
            $sondage->save();
        }
        
    }
    Flight::redirect('/RemyHour');
});

Flight::route('/RemyHour/Commentaires', function(){
    /*var_dump(Flight::request());*/
    $data = [
        'EspaceCom' => GetCom::find_many(),
    ];
    Flight::view()->display('commentaires.twig', $data);
});

Flight::route('/RemyHour/Commentaires/add', function(){

    if (Flight::request()->method == 'POST'){
        $comment = Model::factory('GetCom')->create();
        $comment->pseudo = Flight::request()->data->pseudo;
        $comment->com = Flight::request()->data->com;

        $comment->save();
    }
    Flight::redirect('/RemyHour/Commentaires');
});

Flight::start();